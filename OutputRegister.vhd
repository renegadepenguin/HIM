library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity output_register is
     Port (    clk    : in  std_logic; 
               lo     : in  std_logic; 
               input  : in  std_logic_vector(7 downto 0);
               output : out std_logic_vector(7 downto 0));
end output_register;

--output<=input  lo=0
--output<=output lo=1
architecture behavioral of output_register is
     signal tmp: std_logic_vector(7 downto 0);
begin
     process(clk,lo,input)
     begin
          if (clk'event and clk='1') then
               if lo='1' then
                    tmp<=tmp;
               else
                    tmp<=input;
               end if;
          end if;
     end process;
     output<=tmp;
end behavioral;
