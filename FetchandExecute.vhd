library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fetch is
    Port (	t    : in std_logic_vector(5 downto 0);
               ir   : in std_logic_vector(7 downto 0);
               eclk :out std_logic;
               flags: out std_logic_vector(11 downto 0));
end fetch;

architecture Behavioral of fetch is
signal flags_aux : std_logic_vector(11 downto 0);
signal eclk_aux: std_logic:='1';
-- Flags(11 downto 0): CpEpLm  L1E1LaEa  SuEuLbLo
begin
	process(t,ir)
	begin
          if(t="000000")then
               flags_aux<="00101110X011";
          elsif(t="100000")then --T1 Fetch
               flags_aux<="010111100011";
          elsif(t="010000")then --T2 Fetch
               flags_aux<="101111100011";
          elsif(t="001000")then --T3 Fetch
               flags_aux<="001001100011";
          elsif(t="000100")then --T4 Execute
               if(ir="0000XXXX")then -- LDA
                    flags_aux<="00011010X011";
               elsif(ir="0001XXXX")then -- ADD
                    flags_aux<="00011010X011";
               elsif(ir="0010XXXX")then -- SUB
                    flags_aux<="00011010X011";
               elsif(ir="1110XXXX")then -- OUT
                    flags_aux<="00111111X010";
               elsif(ir="1111XXXX")then -- HLT
                    eclk_aux<='0';
               end if;
          elsif(t="000010")then --T5 Execute
               if(ir="0000XXXX")then -- LDA
                    flags_aux<="00101100X011";
               elsif(ir="0001XXXX")then -- ADD
                    flags_aux<="00101110X001";
               elsif(ir="0010XXXX")then -- SUB
                    flags_aux<="00101110X001";
               elsif(ir="1110XXXX")then -- OUT
                    flags_aux<="00111110X011";
               end if;
          elsif(t="000001")then --T6 Execute
               if(ir="0000XXXX")then -- LDA
                    flags_aux<="00111110X011";
               elsif(ir="0001XXXX")then -- ADD
                    flags_aux<="001111000111";
               elsif(ir="0010XXXX")then -- SUB
                    flags_aux<="001111001111";
               elsif(ir="1110XXXX")then -- OUT
                    flags_aux<="00111110X011";
               end if;
          end if;
	end process;
	flags <= flags_aux;
     eclk  <= eclk_aux;
end Behavioral;

