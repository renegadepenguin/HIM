--Signals:
--
--   eclk: Enable Clk (halt disable everything)
--   endp: End Programming (MAR&INPUT enable this)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HIM is
     Port (    TheClk   : in  std_logic;
               TheEnter : in  std_logic;
               TheProg  : in  std_logic;
               TheInput : in  std_logic_vector(7 downto 0);
               TheOutput: out std_logic_vector(7 downto 0));
end HIM;

architecture Behavioral of HIM is
--Component Declaration
component program_counter is
     Port (    Cp     : in  std_logic;
               Ep     : in  std_logic;
               clk    : in  std_logic;
               --clr    : in  std_logic;
               outbus : out std_logic_vector(3 downto 0));
end component;

component debounce is
    Port ( clk    : in   STD_LOGIC;
           input  : in   STD_LOGIC;
           output : out  STD_LOGIC);
end component;

component MARAndInput is
     Port (    SW_in  : in  std_logic_vector (7 downto 0);
               buff_in: in  std_logic_vector (3 downto 0);
               btn_in : in  std_logic;
               clk    : in  std_logic;
               Lm     : in  std_logic;
		     prog   : in  std_logic;
               addr   : out std_logic_vector (3 downto 0);
	          data   : out std_logic_vector (7 downto 0);
		     endp   : out std_logic);
end component;

component ram is
     Port (    ce      : in  STD_LOGIC;
               endp    : in  STD_LOGIC;
               addr    : in  STD_LOGIC_VECTOR (3 downto 0);
               data    : in  STD_LOGIC_VECTOR (7 downto 0);
               out_bus : out STD_LOGIC_VECTOR (7 downto 0);
					clk	  : in  std_logic);
end component;

component instruction_register is
     Port (    clk    : in  std_logic;
               L1     : in  std_logic;
               E1     : in  std_logic;
               --clr    : in  std_logic;
               input  : in  std_logic_vector(7 downto 0);
               out_cu : out std_logic_vector(3 downto 0);
               out_bu : out std_logic_vector(3 downto 0));
end component;

component Counter is
    Port (	clk : in  std_logic;
               eclk: in  std_logic;
               endp: in  std_logic;
			t   : out std_logic_vector(5 downto 0));
end component;

component fetch is
    Port (	t : in std_logic_vector(5 downto 0);
               ir: in std_logic_vector(7 downto 0);
               eclk:out std_logic;
               flags: out std_logic_vector(11 downto 0));
end component;

component accumulator is
     Port (    clk    : in  std_logic;
               la     : in  std_logic;
               ea     : in  std_logic;
	          input  : in  std_logic_vector(7 downto 0);
               output : out std_logic_vector(7 downto 0);
               outbuff: out std_logic_vector(7 downto 0));
end component;

component adder is
     Port (	su      : in  std_logic;
			eu      : in  std_logic;
			in_a    : in  std_logic_vector(7 downto 0);
			in_b    : in  std_logic_vector(7 downto 0);
			outbuff : out std_logic_vector(7 downto 0));
end component;

component buffer_register is
     Port (    clk    : in  std_logic;
               lb     : in  std_logic;
               input  : in  std_logic_vector(7 downto 0);
               output : out std_logic_vector(7 downto 0));
end component;

component output_register is
     Port (    clk    : in  std_logic;
               lo     : in  std_logic;
               input  : in  std_logic_vector(7 downto 0);
               output : out std_logic_vector(7 downto 0));
end component;


--Signal Declaration
signal Wbus    : std_logic_vector(7 downto 0);
signal flags   : std_logic_vector(11 downto 0);
signal RAMaddr : std_logic_vector(3 downto 0);
signal RAMdata : std_logic_vector(7 downto 0);
signal IR_FX  : std_logic_vector(3 downto 0);
signal T       : std_logic_vector(5 downto 0);
signal ACtoALU : std_logic_vector(7 downto 0);
signal BFtoALU : std_logic_vector(7 downto 0);

signal endp    : std_logic;
signal enter   : std_logic;
signal eclk    : std_logic;

begin
--Port Map
PC:  program_counter port map(flags(11),flags(10),TheClk,Wbus(3 downto 0));	
--DB:  debounce port map(TheClk,TheEnter,Enter);
MI:  MARAndInput port map(TheInput,Wbus(3 downto 0),TheEnter,TheClk,flags(9),TheProg,RAMaddr,RAMdata,endp);	
RM:  ram port map(flags(8),endp,RAMaddr,RAMdata,Wbus,TheClk);
IR:  instruction_register port map(TheClk,flags(7),flags(6),Wbus,IR_FX,Wbus(3 downto 0));
RC:  Counter port map(TheClk,eclk,endp,T);
FX:  fetch port map(T,IR_FX & "XXXX",eclk,flags);
AC:  accumulator port map(TheClk,flags(5),flags(4),Wbus,ACtoALU,Wbus);
AL:  adder port map(flags(3),flags(2),ACtoALU,BFtoALU,Wbus);
BF:  buffer_register port map(TheClk,flags(1),Wbus,BFtoALU);
OU:  output_register port map(TheClk,flags(0),Wbus,TheOutput);

end Behavioral;

