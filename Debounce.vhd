library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity debounce is
    Port ( clk    : in   STD_LOGIC;
           input  : in   STD_LOGIC;
           output : out  STD_LOGIC);
end debounce;

architecture Behavioral of debounce is
signal q1: std_logic:='0';
signal q2: std_logic:='0';
signal q3: std_logic:='0';
begin
	process(clk)
	begin
		if (clk'event and clk = '1') then
			q3 <= input;
			q2 <= q3;
			q1 <= q2;
		end if;
	end process;
	output <= q3 and q2 and q1;
end Behavioral;

