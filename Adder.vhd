library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity adder is
     Port (	su      : in  std_logic;
			eu      : in  std_logic;
			in_a    : in  std_logic_vector(7 downto 0);
			in_b    : in  std_logic_vector(7 downto 0);
			outbuff : out std_logic_vector(7 downto 0));
end adder;

architecture Behavioral of adder is
signal outbuff_aux : std_logic_vector(7 downto 0);
begin
	process(eu, su, in_a, in_b)
	begin
	if(eu = '0')then
		outbuff_aux <= "ZZZZZZZZ";
	else
		if(su = '0')then 
			outbuff_aux <= in_a + in_b;
		else
			outbuff_aux <= in_a + not in_b;
	   	end if;
	end if;
	end process;
	outbuff <= outbuff_aux;
end Behavioral;

