library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity ram is
     Port (    ce      : in  STD_LOGIC;
               endp    : in  STD_LOGIC;
               addr    : in  STD_LOGIC_VECTOR (3 downto 0);
               data    : in  STD_LOGIC_VECTOR (7 downto 0);
               out_bus : out STD_LOGIC_VECTOR (7 downto 0);
					clk : in std_logic);
end ram;

architecture Behavioral of ram is
type ram is array(15 downto 0) of std_logic_vector(7 downto 0);
signal dato: ram;
signal clk_w, clk_r: std_logic;
signal qaux : std_logic_vector(7 downto 0);
begin

	clk_r <= ((not ce) and (endp));
	clk_w <= ((not ce) and clk and (not endp));
	
	process(clk_w)
	begin
		if(clk_w'event and clk_w = '1') then
			dato(conv_integer(addr)) <= data;
		end if;
	end process;
	
	
	lect : process(clk_r)
	begin
			if(clk_r'event and clk_r = '1') then
				qaux <= dato(conv_integer(addr)); 
			end if;
	end process;
	
	alt : process(ce, clk)
	begin
		if(ce = '1')then
			out_bus <= "ZZZZZZZZ";
		elsif (clk'event and clk = '1') then
			out_bus <= qaux;
		end if;
	end process;
	
end Behavioral;
