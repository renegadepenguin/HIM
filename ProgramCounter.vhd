library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity program_counter is
     Port (    Cp     : in  std_logic;
               Ep     : in  std_logic;
               clk    : in  std_logic;
               --clr    : in  std_logic;
               outbus : out std_logic_vector(3 downto 0));
end program_counter;
 
 architecture behavioral of program_counter is
      signal outbus_aux: std_logic_vector(3 downto 0):="0000";
 begin
      process(Cp,clk, outbus_aux) --,clr)
      begin
			if(clk'event and clk='0') then
				if(Cp='1') then
                outbus_aux<=outbus_aux+1;
				end if;
			end if;
		end process;
		
		process(Ep, clk, outbus_aux)
		begin
			if(Ep='0')then
				outbus <= "ZZZZ";
			else
				outbus <= outbus_aux;
			end if;
		end process;
		
end behavioral;
