library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity instruction_register is
     Port (    clk    : in  std_logic;
               L1     : in  std_logic;
               E1     : in  std_logic;
               --clr    : in  std_logic;
               input  : in  std_logic_vector(7 downto 0);
               out_cu : out std_logic_vector(3 downto 0);
               out_bu : out std_logic_vector(3 downto 0));
end instruction_register;

architecture behavioral of instruction_register is
signal bu: std_logic_vector(3 downto 0):="ZZZZ";
signal cu: std_logic_vector(3 downto 0):="ZZZZ";
begin
     process(clk,L1,input,E1)
     begin
          if (clk'event and clk='0')then
               if(L1='0')then --Load
                    cu<=input(7 downto 4);
               end if;
               if(E1='0')then --Out to buff
                    --Implement clr
                    bu<=cu;
               else
                    bu<="ZZZZ";
               end if;
          end if;
     end process;
out_bu<=bu;
out_cu<=cu;
end behavioral;
