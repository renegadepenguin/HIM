library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity Counter is
    Port (	clk : in  std_logic;
               eclk: in  std_logic;
               endp: in  std_logic;
			t   : out std_logic_vector(5 downto 0));
end Counter;

architecture Behavioral of Counter is
signal t_aux : std_logic_vector(5 downto 0):="000000";
begin
process(clk,endp, eclk)
begin
     if(endp='0'or eclk='0') then
          t_aux<="000000";
     elsif(clk'event and clk='0')then 
          if(t_aux="000000")then
               t_aux<="100000";
          elsif(t_aux="100000")then
               t_aux<="010000";
          elsif(t_aux="010000")then
               t_aux<="001000";
          elsif(t_aux="001000")then
               t_aux<="000100";
          elsif(t_aux="000100")then
               t_aux<="000010";
          elsif(t_aux="000010")then
               t_aux<="000001";
          elsif(t_aux="000001")then
               t_aux<="100000";
          end if;
     end if;
end process;
t <= t_aux;
end Behavioral;

